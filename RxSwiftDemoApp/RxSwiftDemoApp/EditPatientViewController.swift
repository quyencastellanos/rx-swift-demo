//
//  AddPatientViewController.swift
//  RxSwiftDemoApp
//
//  Created by Quyen Castellanos on 6/26/17.
//  Copyright © 2017 Quyen Castellanos. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class EditPatientViewController: UITableViewController {

    // IBOutlets
    @IBOutlet weak var submitButton: UIBarButtonItem!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var addressTwoTextField: UITextField!
    @IBOutlet weak var stateTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var zipTextField: UITextField!
    
    // IBActions
    @IBAction func submitTapped(_ sender: Any) {
        guard let viewModel = self.viewModel else {
            preconditionFailure("No AddPatientViewModel has been instantiated")
        }
        self.delegate?.updatePatient(patient: viewModel.getPatient())
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    // Additional class vars
    fileprivate var viewModel:EditPatientViewModel?
    var delegate:PatientListViewController?
    fileprivate var disposeBag:DisposeBag = DisposeBag()
    fileprivate var errorColor:UIColor = UIColor.init(red: 203/255, green: 57/255, blue:46/255, alpha: 0.75)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.createMVVMBindings()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setPatient(patient:Patient) {
        self.viewModel = EditPatientViewModel(patient: patient)
    }
    
    fileprivate func createMVVMBindings () {
        // Create MVVM bindings here
        guard let viewModel = self.viewModel else {
            preconditionFailure("No AddPatientViewModel has been instantiated")
        }
        
        // Text field bindings
        self.firstNameTextField.text = viewModel.firstName.value
        self.lastNameTextField.text = viewModel.lastName.value
        self.addressTextField.text = viewModel.address.value
        self.addressTwoTextField.text = viewModel.addressTwo.value
        self.cityTextField.text = viewModel.city.value
        self.stateTextField.text = viewModel.state.value
        self.zipTextField.text = viewModel.zip.value
        
        // First name bindings
        viewModel.isFirstNameValid.subscribe(onNext: {(isValid) in
            self.firstNameTextField.backgroundColor = isValid ? UIColor.clear : self.errorColor
        }, onError: nil, onCompleted: nil, onDisposed: nil).addDisposableTo(disposeBag)
        
        firstNameTextField.rx.controlEvent([.editingChanged]).subscribe(onNext: {
            viewModel.firstName.value = self.firstNameTextField.text ?? ""
        }).addDisposableTo(disposeBag)
        
        // Last name bindings
        viewModel.isLastNameValid.subscribe(onNext: {(isValid) in
            // TODO: display the last name textfield red or clear depending on if it's valid
        }, onError: nil, onCompleted: nil, onDisposed: nil).addDisposableTo(disposeBag)
        
        lastNameTextField.rx.controlEvent([.editingChanged]).subscribe(onNext: {
            // TODO: update the view model's last name attribute
        }).addDisposableTo(disposeBag)
        
        // Address bindings
        viewModel.isAddressValid.subscribe(onNext: {(isValid) in
            // TODO: display the address textfield red or clear depending on if it's valid
        }, onError: nil, onCompleted: nil, onDisposed: nil).addDisposableTo(disposeBag)
        
        addressTextField.rx.controlEvent([.editingChanged]).subscribe(onNext: {
            // TODO: update the view model's address attribute
        }).addDisposableTo(disposeBag)
        
        // Address two bindings
        
            // TODO: create a binding to display the addressTwo textfield red or clear depending on if it's valid
            
            // TODO: create a binding update the view model's addressTwo attribute
        
        // City bindings

            // TODO: create a binding to display the city textfield red or clear depending on if it's valid
            
            // TODO: create a binding update the view model's city attribute
        
        // State bindings
        
            // TODO: create a binding to display the state textfield red or clear depending on if it's valid
            
            // TODO: create a binding update the view model's state attribute
        
        // Zip code bindings
        
            // TODO: create a binding to display the addressTwo textfield red or clear depending on if it's valid
            
            // TODO: create a binding update the view model's addressTwo attribute
        
        // Submit button binding
        viewModel.submitEnabled.subscribe(onNext: { (isEnabled) in
            self.submitButton.isEnabled = isEnabled
        }, onError: nil, onCompleted: nil, onDisposed: nil).addDisposableTo(disposeBag)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
}
