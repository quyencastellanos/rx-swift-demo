//
//  PatientListViewModel.swift
//  RxSwiftDemoApp
//
//  Created by Quyen Castellanos on 6/26/17.
//  Copyright © 2017 Quyen Castellanos. All rights reserved.
//

import Foundation

class PatientListViewModel {
    var patients:[Patient] = [Patient]()
    
    init () {
        self.patients.append(Patient(patientId: "0", firstName: "John", lastName: "Doe", address: "1234 Hospital Lane", addressTwo: nil, city: "Dallas", state: "TX", zip: "75246"))
        self.patients.append(Patient(patientId: "1", firstName: "Emma", lastName: "Long", address: "3623 McKinney Ave", addressTwo: "Apt. 1", city: "Houston", state: "TX", zip: "77018"))
    }
    
    func updatePatient(patient:Patient) {
        if let patientIndex = self.patients.index(where: {$0.patientId == patient.patientId}) {
            let origPatientId = patient.patientId
            self.patients[patientIndex] = patient
            self.patients[patientIndex].patientId = origPatientId
        }
    }
}
