//
//  PatientListViewController.swift
//  RxSwiftDemoApp
//
//  Created by Quyen Castellanos on 6/26/17.
//  Copyright © 2017 Quyen Castellanos. All rights reserved.
//

import UIKit

class PatientListViewController: UITableViewController {

    // Additional class vars
    fileprivate let viewModel = PatientListViewModel()
    fileprivate var selectedPatient:Patient?
    fileprivate let reuseId:String = "patientCell"
    fileprivate let editPatientSegueId:String = "patientsToEditPatient"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.createMVVMBindings()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func createMVVMBindings() {
        
    }
    
    func updatePatient (patient:Patient) {
        self.viewModel.updatePatient(patient: patient)
        self.tableView.reloadData()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.patients.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseId, for: indexPath)
        cell.textLabel?.text = self.viewModel.patients[indexPath.row].firstName
        return cell
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == self.editPatientSegueId, let destinationVC = segue.destination as? EditPatientViewController, let selectedIndex = self.tableView.indexPathForSelectedRow {
            destinationVC.setPatient(patient: self.viewModel.patients[selectedIndex.row])
            destinationVC.delegate = self
        }
    }

}
