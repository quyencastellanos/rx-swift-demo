//
//  Patient.swift
//  RxSwiftDemoApp
//
//  Created by Quyen Castellanos on 6/26/17.
//  Copyright © 2017 Quyen Castellanos. All rights reserved.
//

import Foundation

class Patient {
    var patientId:String = ""
    var firstName:String = ""
    var lastName:String = ""
    var address:String = ""
    var addressTwo:String?
    var city:String = ""
    var state:String = ""
    var zip:String = ""
    
    init (patientId:String, firstName:String, lastName:String, address:String, addressTwo:String?, city:String, state:String, zip:String) {
        self.patientId = patientId
        self.firstName = firstName
        self.lastName = lastName
        self.address = address
        self.addressTwo = addressTwo
        self.city = city
        self.state = state
        self.zip = zip
    }
}
