//
//  AddPatientViewModel.swift
//  RxSwiftDemoApp
//
//  Created by Quyen Castellanos on 6/26/17.
//  Copyright © 2017 Quyen Castellanos. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class EditPatientViewModel {
    var patientId:String = ""
    var firstName:Variable<String> = Variable<String>("")
    var lastName:Variable<String> = Variable<String>("")
    var address:Variable<String> = Variable<String>("")
    var addressTwo:Variable<String> = Variable<String>("")
    var city:Variable<String> = Variable<String>("")
    var state:Variable<String> = Variable<String>("")
    var zip:Variable<String> = Variable<String>("")
    
    init(patient:Patient) {
        self.patientId = patient.patientId
        self.firstName = Variable<String>(patient.firstName)
        self.lastName = Variable<String>(patient.lastName)
        self.address = Variable<String>(patient.address)
        self.addressTwo = patient.addressTwo != nil ? Variable<String>(patient.addressTwo!) : Variable<String>("")
        self.city = Variable<String>(patient.city)
        self.state = Variable<String>(patient.state)
        self.zip = Variable<String>(patient.zip)
    }
    
    // Regex patterns
    let anyWordRegex = try! NSRegularExpression(pattern: "\\w{1}", options: [])
    let addressRegex = try! NSRegularExpression(pattern: "\\d+\\s+\\w+\\s+\\w", options: [])
    let cityRegex = try! NSRegularExpression(pattern: "\\w+", options: [])
    let stateRegex = try! NSRegularExpression(pattern: "^[A-Z]{2}$", options: [])
    let zipCodeRegex = try! NSRegularExpression(pattern: "\\d{5}", options: [])
    
    func getPatient () -> Patient {
        return Patient(patientId: patientId, firstName: firstName.value, lastName: lastName.value, address: address.value, addressTwo: addressTwo.value, city: city.value, state: state.value, zip: zip.value)
    }
    
    var isFirstNameValid : Observable<Bool> {
        return firstName.asObservable().map({ (val) -> Bool in
            let match = self.anyWordRegex.matches(in: val, options: [], range: NSRange(location: 0, length: val.characters.count))
            return match.count > 0
        })
    }
    
    var isLastNameValid : Observable<Bool> {
        return lastName.asObservable().map({ (val) -> Bool in
            // TODO: Use the anyWordRegex pattern to validate isLastNameValid
            return true
        })
    }
    
    var isAddressValid : Observable<Bool> {
        return address.asObservable().map({ (val) -> Bool in
            // TODO: Use the addressRegex pattern to validate isAddressValid
            return true
        })
    }
    
    var isAddressTwoValid : Observable<Bool> {
        return addressTwo.asObservable().map({ (val) -> Bool in
            return true
        })
    }
    
    var isCityValid : Observable<Bool> {
        return city.asObservable().map({ (val) -> Bool in
            // TODO: Use the cityRegex pattern to validate isCityValid
            return true
        })
    }
    
    var isStateValid : Observable<Bool> {
        return state.asObservable().map({ (val) -> Bool in
            // TODO: Use the stateRegex pattern to validate isStateValid
            return true
        })
    }
    
    var isZipCodeValid : Observable<Bool> {
        return zip.asObservable().map({ (val) -> Bool in
            // TODO: Use the zipCodeRegex pattern to validate isZipCodeValid
            return true
        })
    }
    
    var submitEnabled : Observable<Bool> {
        return Observable.combineLatest([isFirstNameValid, isLastNameValid, isAddressValid, isAddressTwoValid, isCityValid, isStateValid, isZipCodeValid]) { (validations) in
            for isValid in validations {
                if !isValid {
                    return false
                }
            }
            return true
        }
    }

}
